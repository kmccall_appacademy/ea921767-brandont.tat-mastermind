class Code
  PEGS = {
    "R" => :red, "G" => :green, "B" => :blue,
    "Y" => :yellow, "O" => :orange, "P" => :purple
  }

  attr_reader :pegs

  def self.parse(code)
    pegs = code.chars.map do |peg|
      if PEGS.has_key?(peg.upcase)
        PEGS[peg.upcase]
      else
        raise_error "Invalid colors"
      end
    end

    Code.new(pegs)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.values.sample}
    Code.new(pegs)
  end

  def initialize(pegs)
    @pegs = pegs
  end

  def [] (i)
    pegs[i]
  end

  def exact_matches(user_pegs)
    exact_matches = pegs.each_index.select do |i|
      pegs[i] == user_pegs[i]
    end

    exact_matches.size
  end

  def near_matches(user_pegs)
    near_matches = user_pegs.pegs.each_with_index.select do |peg, i|
      pegs.include?(peg) unless pegs[i] == peg
    end

    near_matches.flatten.reject{|el| el.is_a?(Integer)}.uniq.size
  end

  def == (user_pegs)
    user_pegs == pegs
  end
end
