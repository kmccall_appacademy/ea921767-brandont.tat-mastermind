require_relative 'code'

class Game
  MAX_TURNS = 10
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
  end

  def get_guess
    puts "Color choices are R, G, B, Y, O, and P"
    print "Enter 4 colors in the following format XXXX: "
    Code.parse(gets.chomp)
  end

  def display_matches(code)
    exact_match = secret_code.exact_matches(code)
    near_match = secret_code.near_matches(code)

    puts "You got #{exact_match} exact matches"
    puts "You got #{near_match} near matches"
  end

  def play
    MAX_TURNS.times do
      guess = get_guess

      if guess == secret_code
        puts "Congratulations you guessed the secret code, #{secret_code}!"
        return
      else
        display_matches(guess)
      end
    end
    
    puts "Better luck next time"
  end
end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
